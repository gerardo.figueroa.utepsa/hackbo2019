import Vue from 'vue'
import Router from 'vue-router'
import About from './views/About.vue'
import Comments from './views/Comments.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'about',
      component: About
    },
    {
      path: '/comments',
      name: 'comments',
      component: Comments
    }
  ]
})
