import bodyParser from 'body-parser';
import { Server } from '@overnightjs/core';
import swaggerUi from 'swagger-ui-express';
import { specs } from './src/config/swagger.config';
import ControllerConfig from './src/config/controller.config';
import ErrorHandler from './src/config/errorHandler.config';
import cors from 'cors';
import express from 'express';
import { Inject, Singleton } from 'typescript-ioc';

@Singleton
export default class App extends Server {
  constructor(@Inject private controllerConfig: ControllerConfig) {
    super(true);
    this.swaggerConfig();
    this.config();
    this.setupControllers();
  }

  start(): void {
    this.app.listen(3000, () => {
      console.log('Run on port 3000');
    })
  }

  private config(): void {
    this.app.use(cors());
    this.app.use(bodyParser.json());
    this.app.use(express.static('./temp'));

  }

  private setupControllers(): void {
    super.addControllers(this.controllerConfig.getControllers());
    this.app.use(ErrorHandler);
  }

  private swaggerConfig(): void {
    this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
  }
}
