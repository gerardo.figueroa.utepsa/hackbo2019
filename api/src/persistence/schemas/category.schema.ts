import { Schema, model, Types } from 'mongoose';
import { ICategoryDocument } from '../documents/category.document';

const CategorySchema: Schema = new Schema({
  name: {
    required: true,
    type: String,
    unique: true,
  },
  score: {
    required: true,
    type: Number,
  }
}).index({ name: 1, score: 1 });

export default model<ICategoryDocument>('categories', CategorySchema);
