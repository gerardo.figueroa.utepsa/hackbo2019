export interface IErrorModel {
  name: string;
  message: string;
}