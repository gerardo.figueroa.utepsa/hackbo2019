export interface IHttpResponseModel {
  code: number;
  message?: string;
  data?: any[] | any;
}
