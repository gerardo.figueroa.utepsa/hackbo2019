import { ValidationErrorItem } from "joi";

export default function (error: ValidationErrorItem[]): ValidationErrorItem[] {
  error.forEach((e: ValidationErrorItem) => {
    console.log('Error type', e.type);
    switch (e.type) {
      case 'any.empty':
        e.message = 'invalidEmpty';
        break;
      case 'string.email':
        e.message = "invalidEmail";
        break;
      case 'number.base':
        e.message = "invalidDataType";
        break;
      default:
        break;
    }
  })

  return error;
}
