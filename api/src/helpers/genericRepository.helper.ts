import { Model } from 'mongoose';

export abstract class GenericRepository<T> {
  private model: Model<any>;

  constructor (model: Model<any>) {
    this.model = model;
  }

  async findAll (): Promise<T[]> {
    return await this.model.find();
  }

  async findById (id: string): Promise<T> {
    return await this.model.findById(id);
  }

  async save (body: any): Promise<T> {
    return await this.model.create(body);
  }

  async updateById (id: string, body: any): Promise<T> {
    return await this.model.findByIdAndUpdate(id, body, {
      runValidators: true
    });
  }

  async deleteById (id: string): Promise<T> {
    return await this.model.findByIdAndDelete(id);
  }
}
