import { Controller, Middleware, Get, Put, Post, Delete } from '@overnightjs/core';
import { Request, Response, NextFunction } from 'express';
import { Inject } from "typescript-ioc";

import { ICategoryDto } from '../../shared/dtos/category.dto';
import { CategoryService } from './category.service';
import { IHttpResponseModel } from '../../shared/models/httpResponse.model';
import { CategoryQuery } from '../../application/queries/category.query';

import { ValidateBody } from './category.middleware';

@Controller('api/v1/category')
export class CategoryController {
  constructor(
    @Inject private categoryService: CategoryService,
    @Inject private categoryQuery: CategoryQuery
  ) { }

  @Get()
  async getAllCategories (req: Request, res: Response, next: NextFunction) {
    try {
      const categories: ICategoryDto[] = await this.categoryService.getAllCategories();
      const httpResponse: IHttpResponseModel = this.categoryQuery.getAllCategories(categories);
      return res.status(httpResponse.code).send(httpResponse);
    } catch (e) {
      next({ name: e.name, message: e.message });
    }
  }

  @Get(':id')
  async getCategoryById (req: Request, res: Response, next: NextFunction) {
    try {
      const category: ICategoryDto = await this.categoryService.getCategoryById(req.params.id);
      const httpResponse: IHttpResponseModel = this.categoryQuery.getCategoryById(category);
      res.status(httpResponse.code).send(httpResponse);
    } catch (e) {
      next({ name: e.name, message: e.message });
    }
  }

  @Post()
  @Middleware([ValidateBody])
  async postCategory (req: Request, res: Response, next: NextFunction) {
    try {
      const category: ICategoryDto = await this.categoryService.postCategory(req.body);
      const httpResponse: IHttpResponseModel = this.categoryQuery.postCategory(category);
      return res.status(httpResponse.code).send(httpResponse);
    } catch (e) {
      next({ name: e.name, message: e.message })
    }
  }

  @Put(':id')
  @Middleware([ValidateBody])
  async putCategory (req: Request, res: Response, next: NextFunction) {
    try {
      const category: ICategoryDto = await this.categoryService.putCategory(req.params.id, req.body);
      const httpResponse: IHttpResponseModel = this.categoryQuery.putCategory(category);
      return res.status(httpResponse.code).send(httpResponse);
    } catch (e) {
      next({ name: e.name, message: e.message });
    }
  }

  @Delete(':id')
  async deleteCategory (req: Request, res: Response, next: NextFunction) {
    try {
      const category: ICategoryDto = await this.categoryService.deleteCategory(req.params.id);
      const httpResponse: IHttpResponseModel = this.categoryQuery.deleteCategory(category);
      return res.status(httpResponse.code).send(httpResponse);
    } catch (e) {
      next({ name: e.name, message: e.message });
    }
  }
}
