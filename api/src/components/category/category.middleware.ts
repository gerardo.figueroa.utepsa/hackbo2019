import Joi, { ValidationErrorItem, Schema } from 'joi';
import { Request, Response, NextFunction } from 'express';
import { ICategoryDto } from '../../shared/dtos/category.dto';
import ValidationErrorItemHelper from '../../helpers/validationErrorItem.helper';

export async function ValidateBody (req: Request, res: Response, next: NextFunction) {
  try {
    const schema: Schema = defineJoiSchema();
    const result: ICategoryDto = await Joi.validate(req.body, schema);

    req.body = result;
    next();
  } catch (e) {
    next({ name: e.name, message: e.message })
  }
}

function defineJoiSchema (): Schema {
  const schema: Schema = Joi.object().keys({
    name: Joi.string().trim().required().error((error: ValidationErrorItem[]) => {
      return ValidationErrorItemHelper(error);
    }),
    score: Joi.number().required().error((error: ValidationErrorItem[]) => {      
      return ValidationErrorItemHelper(error);
    })
  })

  return schema;
}
