import { GenericRepository } from '../../helpers/genericRepository.helper';
import { ICategoryDto } from '../../shared/dtos/category.dto';

export class CategoryRepository extends GenericRepository<ICategoryDto> {

}