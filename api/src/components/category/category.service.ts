import { CategoryRepository } from './category.repository';
import CategorySchema from '../../persistence/schemas/category.schema';
import { ICategoryDto } from '../../shared/dtos/category.dto';

export class CategoryService {
  private categoryRepository: CategoryRepository;

  constructor () {
    this.categoryRepository = new CategoryRepository(CategorySchema);
  }

  async getAllCategories (): Promise<ICategoryDto[]> {
    const categories: ICategoryDto[] = await this.categoryRepository.findAll();
    return categories;
  }

  async getCategoryById (categoryId: string): Promise<ICategoryDto> {
    return await this.categoryRepository.findById(categoryId);
  }

  async postCategory (category: ICategoryDto): Promise<ICategoryDto> {
    return await this.categoryRepository.save(category);
  }

  async putCategory (categoryId: string, category: ICategoryDto): Promise<ICategoryDto> {
    return await this.categoryRepository.updateById(categoryId, category);
  }

  async deleteCategory (categoryId: string): Promise<ICategoryDto> {
    return await this.categoryRepository.deleteById(categoryId);
  }
}
