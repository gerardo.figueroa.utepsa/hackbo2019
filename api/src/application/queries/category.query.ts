import { ICategoryDto } from '../../shared/dtos/category.dto';
import { IHttpResponseModel } from '../../shared/models/httpResponse.model';
import { Singleton } from 'typescript-ioc';

@Singleton
export class CategoryQuery {
  constructor () { }

  getAllCategories (categories: ICategoryDto[]): IHttpResponseModel {
    if (categories.length === 0)
      return { code: 404, message: 'notFound' };
    return { code: 200, data: categories };
  }

  getCategoryById (category: ICategoryDto): IHttpResponseModel {
    if (category === null)
      return { code: 404, message: 'notFound' }
    return { code: 200, data: category };
  }

  postCategory (category: ICategoryDto): IHttpResponseModel {
    if (category === null)
      return { code: 400, message: 'notCreated' };
    return { code: 201, message: 'createdSuccessfully' }
  }

  putCategory (category: ICategoryDto): IHttpResponseModel {
    if (category === null)
      return { code: 404, message: 'notFound' };
    return { code: 200, message: 'updatedSuccessfully' }
  }

  deleteCategory (category: ICategoryDto): IHttpResponseModel {
    if (category === null)
      return { code: 404, message: 'notFound' };
    return { code: 200, message: 'deletedSuccessfully' };
  }
}
