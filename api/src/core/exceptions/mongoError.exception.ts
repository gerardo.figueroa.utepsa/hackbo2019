import { IErrorModel } from "../../shared/models/error.model";
import { IHttpResponseModel } from "../../shared/models/httpResponse.model";

export default function (error: IErrorModel): IHttpResponseModel {
  return {
    code: 404,
    message: 'duplicatedKey'
  }
}
