import { IErrorModel } from "../../shared/models/error.model";
import { IHttpResponseModel } from "../../shared/models/httpResponse.model";

export default function (error: IErrorModel): IHttpResponseModel {
  if (error.message.includes('allowed'))
    return {
      code: 400,
      message: 'badRequest'
    }
  if (error.message.includes('invalidEmpty'))
    return {
      code: 400,
      message: 'invalidEmpty'
    }
  if (error.message.includes('invalidEmail'))
    return {
      code: 400,
      message: 'invalidEmail'
    }
  if (error.message.includes('invalidDataType'))
    return {
      code: 400,
      message: 'invalidDataType'
    }

  return {
    code: 404,
    message: 'notFound'
  }
}
