import swaggerJsdoc, { Options, SwaggerDefinition, ApiInformation } from 'swagger-jsdoc';

const privateMethods = {
  getSwaggerDefinitions(): SwaggerDefinition {
    let info: ApiInformation = {
      title: 'HackBo 2019',
      description: 'API of HackBo 2019',
      version: '1.0.0'
    };

    return {
      basePath: '/api/v1',
      host: 'localhost:3000',
      info
    }
  }
}

const options: Options = {
  swaggerDefinition: privateMethods.getSwaggerDefinitions(),
  apis: ['./src/core/swagger/*'],
};

export const specs = swaggerJsdoc(options);