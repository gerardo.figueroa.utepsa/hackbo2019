import { Inject, Singleton } from 'typescript-ioc';
import { CategoryController } from '../components/category/category.controller';

@Singleton
export default class ControllerConfig {
  constructor(
    @Inject private categoryController: CategoryController
  ) { }

  getControllers(): any[] {
    return [
      this.categoryController
    ];
  }
}