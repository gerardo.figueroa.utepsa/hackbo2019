import { Request, Response, NextFunction } from 'express';
import { IErrorModel } from '../shared/models/error.model';
import CastErrorException from '../core/exceptions/castError.exception';
import ValidationErrorException from '../core/exceptions/validationError.exception';
import MongoErrorException from '../core/exceptions/mongoError.exception';
import { IHttpResponseModel } from '../shared/models/httpResponse.model';

export default function (error: IErrorModel, req: Request, res: Response, next: NextFunction) {
  let httpResponse: IHttpResponseModel;
  console.log(error);

  if (error.name === 'CastError') {
    httpResponse = CastErrorException(error);
    return res.status(httpResponse.code).send(httpResponse);
  }
  if (error.name === 'ValidationError') {
    httpResponse = ValidationErrorException(error);
    return res.status(httpResponse.code).send(httpResponse);
  }
  if (error.name === 'MongoError') {
    httpResponse = MongoErrorException(error);
    return res.status(httpResponse.code).send(httpResponse);
  }
}