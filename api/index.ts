import Aplication from './app';
import Db from './src/config/db.config';
import { Inject, Singleton } from 'typescript-ioc';

@Singleton
class Index {
  @Inject private aplication: Aplication;

  constructor() { }

  dbConnect(): void {
    Db.connect();
    this.start();
  }

  private start(): void {
    this.aplication.start()
  }
}

let index: Index = new Index();
index.dbConnect();
